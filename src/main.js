import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import {createRouter, createWebHistory} from "vue-router";
import store from './store';
import Tasks from "@/components/Tasks.vue";
import Home from "@/components/Home.vue";


const app = createApp(App)
const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home,
    },
    {
        name: 'Tasks',
        path: '/tasks',
        component: Tasks
    }
]

const router = createRouter({
    mode: 'history',
    history: createWebHistory(),
    routes,
    linkActiveClass: '',
})

app.use(router)
app.use(store)
app.mount('#app')

export default router

