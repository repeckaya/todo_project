import { createStore } from 'vuex';

const store = createStore({
    state: {
        tasks: [],
    },
    mutations: {
        addTask(state, task) {
            state.tasks.push(task);
        },
        editTask(state, { taskId, taskName }) {
            const task = state.tasks.find(t => t.id === taskId);
            if (task) {
                task.name = taskName;
            }
        },
        deleteTask(state, taskId) {
            state.tasks = state.tasks.filter(t => t.id !== taskId);
        },
        setTasks(state, tasks) {
            state.tasks = tasks;
        },
    },
    actions: {
        saveTasks({ state }) {
            localStorage.setItem('tasks', JSON.stringify(state.tasks));
        },
        loadTasks({ commit }) {
            const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
            commit('setTasks', tasks);
        },
    },
    plugins: [
        store => {
            store.subscribe((mutation, state) => {
                if (mutation.type !== 'setTasks') {
                    store.dispatch('saveTasks');
                }
            });
            store.dispatch('loadTasks');
        },
    ],
});

export default store;
